<?php
    
    namespace App\Controller;

    use App\Entity\Article;

    use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\Annotation\Route;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;


    Class ArticleController extends Controller {
       
    /** 
    * @Route("/article", name="article_list")
    * @Method({"GET"})
    */
        public function index(){
            $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
            $success = true;
            $errors  = [];
            $status  = 200; 
            return $this->json(['success'=> $success,
                                'errors' => $errors,
                                 'data'   => $articles,
                                ],$status); 


        }
       
        /**
         * @Route("/article/new", name="new_article")
        * @Method({"GET", "POST"})
         */
        public function new(Request $request){

        $success = true;
        $errors  = [];
        $status  = 200; 
            try {
                $request = Request::createFromGlobals();
                $params = array();
                $content = $request->getContent();
                if (empty($content)) {
                    throw new \Exception('Data was not received.', 400);
                }
                $params = json_decode($content, true); // 2nd param to get as array
                $entityManager = $this->getDoctrine()->getManager();
                $article = new Article();
                $article->setTitle($params['title']);
                $article->setBody($params['body']);
                
                $entityManager->persist($article);
                $entityManager->flush();
          
            } catch (\Exception $ex) {
                $success = false;
                $errors[] = $ex->getMessage();
            }
           
                return $this->json(['success'=> $success,
                                'errors' => $errors,
                                'data'   => $article,
                               ],$status); 
  
        }


         /**
         * @Route("/article/edit/{id}", name="edit_article")
         * @Method({"GET", "POST"})
         */
        public function edit(Request $request, $id){
        $success = true;
        $errors  = [];
        $status  = 200; 
            try {
                $request = Request::createFromGlobals();
                $params = array();
                $content = $request->getContent();
                if (empty($content)) {
                    throw new \Exception('Data was not received.', 400);
                }
                $params = json_decode($content, true); // 2nd param to get as array
                $entityManager = $this->getDoctrine()->getManager();
                $article = new Article();
                $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
                $article->setTitle($params['title']);
                $article->setBody($params['body']);
                
                $entityManager->persist($article);
                $entityManager->flush();
          
            } catch (\Exception $ex) {
                $success = false;
                $errors[] = $ex->getMessage();
            }
           
                return $this->json(['success'=> $success,
                                'errors' => $errors,
                                'data'   => $article,
                               ],$status); 
        }

        /**
        * @Route("/article/{id}", name="article_show")
        */
        public function show($id) {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        return $this->json(array('article' => $article));
      }


      /**
       * @Route("/article/delete/{id}")
       */
        public function delete(Request $request, $id){
            $article = $this->getDoctrine()->getRepository(Article::class)->find($id);

            $entityManager = $this->getDoctrine()->getManager();  
            $entityManager->remove($article);
            $entityManager->flush();

            $response = new Response();
            $response->send();
            return $this->redirectToRoute('article_list');

            

        }      
    } 









//        /**
//         * @Route("/article/save")
//         */
//      public function save(){
//            $entityManager =  $this->getDoctrine()->getManager();
//
//            $article = new Article();
//            $article->setTitle('Article Tow');
//            $article->setBody('This is the body for article tow');
//
//            $entityManager->persist($article);
//
//            $entityManager->flush($article);
//
//            return new Response('Saved articles whith the id of '.$article->getId());
//        }